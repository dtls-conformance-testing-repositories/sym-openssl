read -p 'Constraint: ' rulevar
read -p 'Fragmentation (0/1):' fragvar


if [ $fragvar = '0' ] 
then
    directory=handshakes/nofrag
    fragflag=
elif [ $fragvar -eq '1' ] 
then
    directory=handshakes/frag
    fragflag=-f
else
    echo 'Wrong Fragmentation Option'
    exit -1
fi

echo "./dtls-fuzz.bc -i $directory -r $rulevar -c psk $fragflag"
/home/hooman/Repos/symbolicexecution/klee/build/bin/klee --simplify-sym-indices --write-cov -disable-inlining --optimize --use-forked-solver --use-cex-cache --libc=uclibc --posix-runtime --only-output-states-covering-new --external-calls=all --max-memory-inhibit=false --search=random-path --search=nurs:covnew --max-memory=6000 --max-sym-array-size=4096 --solver-backend=z3 --max-solver-time=10s -write-sym-paths -max-time=24h -output-dir=$rulevar ./dtls-fuzz.bc -i $directory -r $rulevar -c psk $fragflag
